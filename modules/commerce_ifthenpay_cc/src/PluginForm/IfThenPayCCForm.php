<?php

namespace Drupal\commerce_ifthenpay_cc\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Payment redirect form for Ifthenpay CC.
 */
class IfThenPayCCForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  const CC_LIVE_URL = 'https://ifthenpay.com/api/creditcard/init/';

  const CC_TEST_URL = 'https://ifthenpay.com/api/creditcard/sandbox/init/';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;


  /**
   *
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('http_client'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $gateway_mode = $payment_gateway_plugin->getMode();
    $configuration = $payment_gateway_plugin->getConfiguration();

    if ($gateway_mode == 'live') {
      $uri = self::CC_LIVE_URL;
    }
    else {
      $uri = self::CC_TEST_URL;
    }

    $data = [
      'orderId' => $payment->getOrderId(),
      'amount' => $payment->getAmount()->getNumber(),
      'successUrl' => $form['#return_url'],
      'errorUrl' => $form['#cancel_url'] . '?status=error',
      'cancelUrl' => $form['#cancel_url'],
    ];

    try {

      $params = [
        'json' => $data,
        'headers' => [
          'Content-Type' => 'application/json',
        ],
      ];

      $response = $this->httpClient->post($uri . $configuration["cccard_key"], $params);
    } catch (ClientException $e) {
      throw $e;
    }

    $response_data = Json::decode((string) $response->getBody());

    // "Message": "Success"
    if ($response_data["Status"] == '0') {
      $redirect_url = $response_data["PaymentUrl"];
    }
    else {
      throw new \Exception($response_data["Message"], $response_data["Status"]);
    }


    if (!empty($redirect_url)) {

      $payment->setRemoteId($response_data["RequestId"]);
      $payment->setState('authorization')->save();

      return $this->buildRedirectForm($form, $form_state, $redirect_url, [], BasePaymentOffsiteForm::REDIRECT_POST);
    }

  }

}
