<?php

namespace Drupal\commerce_ifthenpay_cc\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Commerce Ifthenpay Credit Card Redirect payment gateway.
 * Docs:
 * https://docs.drupalcommerce.org/commerce2/developer-guide/payments/create-payment-gateway/off-site-gateways/off-site-redirect
 *
 * @CommercePaymentGateway(
 *   id = "ifthenpay_cc",
 *   label = "Ifthenpay - Credit Card",
 *   display_label = "Credit Card",
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_ifthenpay_cc\PluginForm\IfThenPayCCForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "maestro", "mastercard", "visa",
 *   },
 * )
 */
class IfthenpayCC extends OffsitePaymentGatewayBase {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $gateway */
    $gateway = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $gateway->httpClient = $container->get('http_client');
    return $gateway;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'cccard_key' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['cccard_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CCARD Key'),
      '#default_value' => $this->configuration['cccard_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['cccard_key'] = $values['cccard_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {

    $status = $request->get('status');
    $remote_id = $request->get('requestId');
    $charged_amount = $request->get('amount');

    /** @var \Drupal\commerce_payment\PaymentStorage $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = $payment_storage->loadByRemoteId($remote_id);

    // If the errorUrl throw a \Drupal\commerce_payment\Exception\PaymentGatewayException
    // and that will abort the order checkout and redirect to previous checkout step
    // rather to completion page
    if ($status === 'error') {
      $payment->delete();
      throw new PaymentGatewayException("Failed");
    }

    $logger = \Drupal::logger('commerce_ifthenpay_cc');
    $order_amount = $order->getTotalPrice()->getNumber();
    if ($order_amount != $charged_amount) {
      $payment->delete();
      $logger->warning('Charged Amount is: ' . $charged_amount . ' while Order Amount: ' . $order_amount);
      throw new PaymentGatewayException('Charged amount not equal to order amount.');
    }
    $payment->setState('completed')->save();
    $this->messenger($this->t('Payment was processed'));
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
  }


}
