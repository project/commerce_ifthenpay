<?php

namespace Drupal\commerce_ifthenpay_mbway\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the payment method type for MBWAY payments.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_ifthenpay_mbway",
 *   label = @Translation("MBWay"),
 *   create_label = @Translation("MBWay"),
 * )
 */
class Mbway extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $mbway_number = NULL;
    if ($payment_method->hasField('mbway_number')) {
      $mbway_number = $payment_method->mbway_number->value;
    }
    $placeholders = [
      '@mbway_number' => $mbway_number,
    ];
    return $this->t('MB Way number: @mbway_number', $placeholders);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['mbway_number'] = BundleFieldDefinition::create('string')
      ->setLabel(t('MB Way Mobile number'))
      ->setDescription(t('The MB Way mobile number.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
