<?php

namespace Drupal\commerce_ifthenpay_mbway\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the Mbway payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_ifthenpay_mbway",
 *   label = @Translation("Mb Way"),
 *
 * )
 */
class PaymentMbway extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
