<?php

namespace Drupal\commerce_ifthenpay_mbway\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Ifthenpay MBWAY payment gateway.
 *
 * Because the only payment gateways that can display fields on checkout are
 * ones that are an instance of 'SupportsStoredPaymentMethodsInterface'
 * (https://www.drupal.org/project/commerce/issues/2894961) I created a custom
 * payment method like https://www.drupal.org/project/commerce_purchase_order
 *
 * @CommercePaymentGateway(
 *   id = "ifthenpay_mbway",
 *   label = "Ifthenpay - MB WAY",
 *   display_label = "MB WAY",
 *   modes = {
 *     "n/a" = @Translation("N/A"),
 *   },
 *   forms = {
 *     "add-payment" =
 *   "Drupal\commerce_ifthenpay_mbway\PluginForm\MbwayPaymentMethodAddForm",
 *     "receive-payment" =
 *   "Drupal\commerce_ifthenpay_mbway\PluginForm\PaymentReceiveForm",
 *   "add-payment-method" =
 *   "Drupal\commerce_ifthenpay_mbway\PluginForm\MbwayPaymentMethodAddForm"
 *   },
 *   payment_method_types = {"commerce_ifthenpay_mbway"},
 *   payment_type = "payment_ifthenpay_mbway",
 *   requires_billing_information = FALSE,
 *   )
 */
class IfthenpayMbway extends PaymentGatewayBase implements OnsitePaymentGatewayInterface, HasPaymentInstructionsInterface, SupportsVoidsInterface, ContainerFactoryPluginInterface, SupportsNotificationsInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, ClientInterface $client, RounderInterface $rounder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->httpClient = $client;
    $this->rounder = $rounder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('http_client'),
      $container->get('commerce_price.rounder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'instructions' => [
          'value' => '',
          'format' => 'plain_text',
        ],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['mbway_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MbWayKey'),
      '#default_value' => $this->configuration['mbway_key'] ?? '',
      '#required' => TRUE,
    ];

    $form['callback_antiphishing'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chave AntiPhishing'),
      '#default_value' => $this->configuration['callback_antiphishing'] ?? '',
      '#required' => TRUE,
    ];

    $form['instructions'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Payment instructions'),
      '#description' => $this->t('Shown the end of checkout, after the customer has placed their order.'),
      '#default_value' => $this->configuration['instructions']['value'] ?? '',
      '#format' => $this->configuration['instructions']['format'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['mbway_key'] = $values['mbway_key'];
      $this->configuration['callback_antiphishing'] = $values['callback_antiphishing'];
      $this->configuration['instructions'] = $values['instructions'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [];
    $amount = $payment->getAmount();
    $payment_method = $payment->getPaymentMethod();
    $mbway_number = $payment_method->mbway_number->value;

    if (!empty($this->configuration['instructions']['value'])) {
      $instructions['intro'] = [
        '#type' => 'processed_text',
        '#text' => $this->configuration['instructions']['value'],
        '#format' => $this->configuration['instructions']['format'],
      ];
    }

    $instructions['mbway_instructions'] = [
      '#theme' => 'mbway_instructions',
      '#mbway_number' => $mbway_number,
      '#amount' => $amount,
    ];


    return $instructions;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->value;
    $operations = [];
    $operations['receive'] = [
      'title' => $this->t('Receive'),
      'page_title' => $this->t('Receive payment'),
      'plugin_form' => 'receive-payment',
      'access' => $payment_state == 'pending',
    ];
    $operations['void'] = [
      'title' => $this->t('Void'),
      'page_title' => $this->t('Void payment'),
      'plugin_form' => 'void-payment',
      'access' => $payment_state == 'pending',
    ];
    return $operations;
  }


  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);

    $order_id = $payment->getOrderId();
    $amount = $payment->getAmount()->getNumber();

    if ($amount < 0.1) {
      $this->messenger()
        ->addWarning($this->t('Mb Way does not allow payments under 0.10€'));
    }
    $payment_method = $payment->getPaymentMethod();
    $mbway_number = $payment_method->mbway_number->value;
    $form_params = [
      'MbWayKey' => $this->configuration['mbway_key'],
      'canal' => '03',
      'referencia' => $order_id,
      'valor' => $amount,
      'nrtlm' => $mbway_number,
      'email' => '',
      'descricao' => '',
    ];

    $request = $this->httpClient->post('https://mbway.ifthenpay.com/IfthenPayMBW.asmx/SetPedidoJSON', [
      'verify' => TRUE,
      'form_params' => $form_params,
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
      ],
    ])->getBody()->getContents();

    $response = json_decode($request);

    if ($response->Estado !== '000') {
      throw new HardDeclineException('The payment failed. Please contact a site administrator.');
    }

    $payment->setRemoteId($response->IdPedido);

    $this->assertPaymentMethod($payment_method);
    $payment->setState('authorization');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['pending']);

    // If not specified, use the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $payment->state = 'completed';
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $payment->state = 'voided';
    $payment->save();
  }


  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      'mbway_number',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $payment_method->setReusable(FALSE);
    $payment_method->mbway_number = $payment_details['mbway_number'];
    $payment_method->save();
  }

  /**
   * @inheritdoc
   */
  public function onNotify(Request $request) {

    $ifthenpay_chave = $request->get('chave');

    if (!empty($this->configuration['callback_antiphishing']) && $this->configuration['callback_antiphishing'] != $ifthenpay_chave) {
      // Return empty response with 403 status code.
      return new Response('', 403);
    }

    $state = $request->get('estado');
    $remote_id = $request->get('idpedido');

    /** @var \Drupal\commerce_payment\PaymentStorage $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = $payment_storage->loadByRemoteId($remote_id);

    if (!is_null($payment) && $state === 'PAGO') {
      /** @var \Drupal\commerce_payment\Entity\Payment $payment */
      $payment->setState('completed')->save();
    }
    else {
      // log not found
      \Drupal::logger('commerce_ifthenpay_mbway')
        ->notice('idpedido not found: @remote_id',
          [
            '@reference' => $remote_id,
          ]);
    }

    // Return empty response with 200 status code.
    return new Response();
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl() {
    return Url::fromRoute('commerce_payment.notify', [
      'commerce_payment_gateway' => $this->parentEntity->id(),
    ], [
      'absolute' => TRUE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // There is no remote system.  These are only stored locally.
    $payment_method->delete();
  }


}


