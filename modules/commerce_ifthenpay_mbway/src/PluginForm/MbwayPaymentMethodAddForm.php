<?php

namespace Drupal\commerce_ifthenpay_mbway\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentMethodFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;

/**
 * PaymentMethodAddForm for MBWAY.
 */
class MbwayPaymentMethodAddForm extends PaymentMethodFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */

    $form['payment_details'] = [
      '#parents' => array_merge($form['#parents'], ['payment_details']),
      '#type' => 'container',
      '#payment_method_type' => 'payment_ifthenpay_mbway',
    ];
    $form['payment_details'] = $this->buildMbwayForm($form['payment_details'], $form_state);

    // Move the billing information below the payment details.
    if (isset($form['billing_information'])) {
      $form['billing_information']['#weight'] = 10;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $values = $form_state->getValue($form['#parents']);
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    // The payment method form is customer facing. For security reasons
    // the returned errors need to be more generic.
    // The Purchase Order Gateway will handle processing the form values.
    try {
      $payment_gateway_plugin->createPaymentMethod($payment_method, $values['payment_details']);
    } catch (DeclineException $e) {
      \Drupal::logger('commerce_payment')->warning($e->getMessage());
      throw new DeclineException('We encountered an error processing your payment method. Please verify your details and try again.');
    } catch (PaymentGatewayException $e) {
      \Drupal::logger('commerce_payment')->error($e->getMessage());
      throw new PaymentGatewayException('We encountered an unexpected error processing your payment method. Please try again later.');
    }
  }

  /**
   * Builds the purchase order form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built form.
   */
  protected function buildMbwayForm(array $element, FormStateInterface $form_state) {
    $element['#attributes']['class'][] = 'mbway-form';
    $element['mbway_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Mobile Number'),
      '#description' => $this->t('After reviewing the order, you will be able to finalize the payment process through the MB WAY App.'),
      '#required' => TRUE,
    ];
    return $element;
  }

  /**
   * Handles the submission of the billing profile form.
   *
   * @param array $element
   *   The billing profile form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function submitBillingProfileForm(array $element, FormStateInterface $form_state) {
    $billing_profile = $element['#entity'];
    $form_display = EntityFormDisplay::collectRenderDisplay($billing_profile, 'default');
    $form_display->extractFormValues($billing_profile, $element, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_method->setBillingProfile($billing_profile);
  }

}
