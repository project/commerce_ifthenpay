CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a Drupal Commerce payment method for the Portuguese payment
gateway Ifthenpay.

Ifthenpay gateway allows customers to generate their own Multibanco References
for each Commerce order. Clients can then pay by debit card, via the ATM network
– Multibanco.

Multibanco is an interbank network in Portugal owned and operated by Sociedade
Interbancaria de Serviços S.A. (SIBS), that links the ATMs of all the banks in
Portugal. The bank members of Multibanco control the SIBS. Multibanco is a
-integrated interbank network and offers more services than the usually found in
other countries, making that one of its most known characteristics. Multibanco
in itself does not only encompass ATMs. It has a full-fledged EFTPOS network
called Multibanco Automatic Payment, and is also a provider of mobile phone and
Internet banking services through the TeleMultibanco, MBNet and most recently
MBWay services.

To get the automatic change of payment state, you need to send an email to
Ifthenpay asking them to activate that feature. The callbak URL format is:

https://[your-domain]/payment/notify/[payment-gateway-machine-name]

The integration with the MBWay payment method is currently under development.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_ifthenpay

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/commerce_ifthenpay


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Drupal Commerce - https://www.drupal.org/project/commerce


INSTALLATION
------------

 * Install the Commerce Ifthenpay module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Commerce > Configuration > Payment gateways
       and click on "Add payment gateway".
    3. To get the automatic change of payment state, you need to send an email to
       Ifthenpay asking them to activate that feature. The callbak URL format is:
       https://[your-domain]/payment/notify/[payment-gateway-machine-name]


MAINTAINERS
-----------

 * Jose Fernandes (introfini) - https://www.drupal.org/u/introfini

Supporting organizations:

 * Bloomidea - https://www.drupal.org/bloomidea
